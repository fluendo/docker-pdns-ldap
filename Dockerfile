FROM alpine:3.7
MAINTAINER fluendo

RUN apk add --no-cache \
    pdns \
    pdns-backend-ldap

COPY docker-cmd.sh /

EXPOSE 53 53/udp

CMD [ "/docker-cmd.sh" ]
